extends Node

func _ready():
	#JavaScript.eval('async defer src="https://connect.facebook.net/en_US/sdk.js"')
	#JavaScript.eval('async defer src="https://connect.facebook.net/es_LA/sdk.js"')
	pass # Replace with function body.


func _on_Button_pressed():
	var cadena:String = $Control/TextEdit.text
	JavaScript.eval(cadena, true)
	pass

func _on_btnLogin_pressed():
	JavaScript.eval("window.FB.login(function(response) {" +\
		"if (response.authResponse) {" +\
		"console.log('Welcome!  Fetching your information.... ');" +\
		"window.FB.api('/me', function(response) {" +\
		"console.log('Good to see you, ' + response.name + '.');" +\
		"});" +\
		"} else {" +\
		"console.log('User cancelled login or did not fully authorize.');" +\
		"}" +\
		"});")
		
	pass # Replace with function body.


func _on_btnShare_pressed():
	JavaScript.eval("window.FB.ui({" + \
  			"method: 'share'," +\
  			"href: 'https://developers.facebook.com/docs/'" +\
			"}, function(response){});")
	pass # Replace with function body.

func _on_btnLikes_pressed():
	JavaScript.eval("window.FB.ui({" +\
		  "method: 'share_open_graph'," +\
		  "action_type: 'og.likes'," +\
		  "action_properties: JSON.stringify({" +\
		  "object:'https://developers.facebook.com/docs/'," +\
		  "})" +\
		"}, function(response){});")
		
	pass

func _on_btnFeed_pressed():
	JavaScript.eval("FB.ui({"+\
		  "method: 'feed'," +\
		  "link: 'https://developers.facebook.com/docs/'" +\
		"}, function(response){});")
	pass

func _on_btnScores_pressed():
	JavaScript.eval("FBInstant"+\
	  ".getLeaderboardAsync('my_awesome_leaderboard.' + context.getID())"+\
	  ".then(leaderboard => {" +\
	    "console.log(leaderboard.getName());" +\
	    "return leaderboard.setScoreAsync(42, '{race: 'elf', level: 3}');"+\
	  "})"+\
	  ".then(() => console.log('Score saved'))"+\
	  ".catch(error => console.error(error));")
	pass

